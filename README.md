NOTES
* Ships with unknown crew or cost are ignored
* Not much error handling is done, the util is expected to fail and exit with exception stack trace if any error occurs
* Developed and tested with Java 15

HOW TO BUILD & RUN

* Install maven 3
* mvn clean package
* java -jar target/swapi-jar-with-dependencies.jar

RESULTS

![SCREENSHOT](/uploads/b3f3342852bb977bbd6750889eac3c07/Screen_Shot_2021-02-08_at_0.46.17.png)
