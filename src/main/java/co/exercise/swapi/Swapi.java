package co.exercise.swapi;

import co.exercise.swapi.pojo.Result;
import co.exercise.swapi.pojo.SwapiResult;
import co.exercise.swapi.utils.Json;
import co.exercise.swapi.utils.Strings;
import io.netty.channel.ChannelOption;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import reactor.core.publisher.Mono;
import reactor.netty.ByteBufMono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.client.HttpClientResponse;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static co.exercise.swapi.utils.Http.*;

public class Swapi {
    private static final Logger log = LogManager.getLogger(Swapi.class);

    public static final String URL = "https://swapi.dev/api/starships";

    public static void main(String[] args) {
        log.info("Getting ships..");

        var ships = getAllShips();
        var smallShips = findSmallShips(ships);

        printShipInfo(ships, smallShips);
    }

    private static void printShipInfo(List<Result> ships, List<Result> smallShips) {
        log.info(String.format("Total ships: %s", ships.size()));
        log.info(String.format("Small ships: %s", smallShips.size()));
        log.info(String.format("%1$-30s%2$-6s%3$-1s", "Name", "Crew", "Cost"));

        smallShips.forEach(ship -> {
            log.info(String.format("%1$-30s%2$-6s%3$-1s", ship.name, ship.crew, ship.cost_in_credits));
        });
    }

    private static List<Result> findSmallShips(List<Result> ships) {
        return ships.stream()
                .filter(ship -> {
                    // filters out ships with unknown crew size or cost
                    var crewCount = getCrewCount(ship.crew);
                    var cost = Strings.parseLong(ship.cost_in_credits);

                    return cost != null && crewCount != null && crewCount < 10;
                })
                .sorted(Comparator.comparingLong(ship -> Strings.parseLong(ship.cost_in_credits)))
                .collect(Collectors.toList());
    }

    private static List<Result> getAllShips() {
        var ships = new ArrayList<Result>();

        var result = getShips(URL);
        ships.addAll(result.results);

        log.info(String.format("Got %s ships", result.results.size()));

        while (StringUtils.isNotBlank(result.next)) {
            result = getShips(result.next);

            log.info(String.format("Got %s ships", result.results.size()));

            if (result.count > 0) {
                ships.addAll(result.results);
            }
        }

        return ships;
    }

    private static Long getCrewCount(String crew) {
        var clean = StringUtils.replace(crew, ",", "");

        if (StringUtils.contains(clean, '-')) {
            String[] split = StringUtils.split(clean, '-');
            return Strings.parseLong(split[0]);
        }

        return Strings.parseLong(clean);
    }

    private static SwapiResult getShips(String url) {
        String response = HttpClient.create()
                .headers(h -> headers(h))
                .secure(spec -> spec.sslContext(SSL_CTX_BUILDER))
                .compress(true)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, CONNECT_TIMEOUT_MS)
                .followRedirect(true)
//                .wiretap(true)
                .responseTimeout(RESPONSE_TIMEOUT)
                .get()
                .uri(url)
                .responseSingle((resp, respContent) -> handleResponseStatus(resp, respContent))
                .block();

        return Json.fromJsonString(response, SwapiResult.class);
    }

    private static Mono<String> handleResponseStatus(HttpClientResponse resp, ByteBufMono respContent) {
        if (HttpResponseStatus.OK.equals(resp.status())) {
            return respContent.asString();
        }

        String errMessage = String.format("Swapi API returned error HTTP status [%s] with headers [%s]",
                resp.status(),
                resp.responseHeaders().toString());

        log.error(errMessage);

        return Mono.error(new RuntimeException(errMessage));
    }
}
