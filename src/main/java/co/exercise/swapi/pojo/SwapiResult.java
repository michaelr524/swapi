package co.exercise.swapi.pojo;

import java.util.List;

public class SwapiResult {
    public int count;
    public String next;
    public List<Result> results;
}
