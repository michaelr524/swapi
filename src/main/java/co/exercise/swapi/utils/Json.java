package co.exercise.swapi.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Json {
    private static final Logger log = LogManager.getLogger(Json.class);

    public static <T> T fromJsonString(String jsonStr, Class<T> clazz) {
        ObjectMapper jsonMapper = new ObjectMapper();

        jsonMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        jsonMapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);

        T result = null;

        try {
            result = jsonMapper.readValue(jsonStr, clazz);
        } catch (Throwable e) {
            log.info("error parsing JSON", e);
        }

        return result;
    }
}
