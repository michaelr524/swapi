package co.exercise.swapi.utils;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.ssl.SslContextBuilder;

import java.time.Duration;

public class Http {
    public static final SslContextBuilder SSL_CTX_BUILDER = SslContextBuilder.forClient();
    public static final int CONNECT_TIMEOUT_MS = 20000;
    public static final Duration RESPONSE_TIMEOUT = Duration.ofMillis(60000);

    public static HttpHeaders headers(HttpHeaders h) {
        return h.set(HttpHeaderNames.CONTENT_TYPE, "application/json");
    }
}
